package app.ricky.morty.data.entity.response.chapter

data class ChapterResponseEntity(val info: InfoEntity, val results: List<ChapterEntity>)
