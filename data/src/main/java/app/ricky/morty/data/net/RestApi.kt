package app.ricky.morty.data.net

import app.ricky.morty.data.entity.response.chapter.ChapterResponseEntity
import app.ricky.morty.data.entity.response.character.CharacterListResponseEntity
import io.reactivex.Observable

interface RestApi{
    /**
     * Retrieves an [Observable] which will emit a [ChapterResponseEntity]
     */
    fun chapterEntityList(next: String): Observable<ChapterResponseEntity>

    /**
     * Retrieves an [Observable] which will emit a [CharacterListResponseEntity].
     *
     * @param characterIds The character ids used to get character data.
     */
    fun characterEntityList(characterIds: String): Observable<CharacterListResponseEntity>
}