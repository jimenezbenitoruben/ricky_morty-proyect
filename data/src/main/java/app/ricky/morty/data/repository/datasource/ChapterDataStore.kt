package app.ricky.morty.data.repository.datasource

import app.ricky.morty.data.entity.response.chapter.ChapterResponseEntity
import app.ricky.morty.data.entity.response.character.CharacterListResponseEntity
import io.reactivex.Observable

interface ChapterDataStore {
    /**
     * Get an [Observable] which will emit a [ChapterResponseEntity].
     */
    fun chapterEntityList(next: String): Observable<ChapterResponseEntity>

    /**
     * Get an [Observable] which will emit a [CharacterListResponseEntity] by its id.
     *
     * @param characterIds The ids to retrieve all characters data.
     */
    fun characterEntityList(characterIds: String): Observable<CharacterListResponseEntity>
}