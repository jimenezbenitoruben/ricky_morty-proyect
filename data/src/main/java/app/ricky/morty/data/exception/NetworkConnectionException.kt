package app.ricky.morty.data.exception

class NetworkConnectionException constructor(): Exception() {
    constructor(cause: Throwable?) : this()
}