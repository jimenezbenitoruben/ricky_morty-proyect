package app.ricky.morty.data.entity.response.character

class CharacterEntity (
        val id: Long,
        val name: String,
        val status: String,
        val species: String,
        val type: String,
        val gender: String,
        val image: String,
        val episode: List<String>,
        val url: String,
        val created: String)