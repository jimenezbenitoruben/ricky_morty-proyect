package app.ricky.morty.data.repository.datasource

import android.content.Context
import app.ricky.morty.data.entity.mapper.ChapterEntityJsonMapper
import app.ricky.morty.data.net.RestApiImpl
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChapterDataStoreFactory @Inject constructor(val context: Context/*, val chapterCache: ChapterCache*/) {

//    /**
//     * Create [ChapterDataStore] from a user id.
//     */
//    fun create(/*chapterId: Int*/): ChapterDataStore {
//        val chapterDataStore: ChapterDataStore
//
//        //TODO:Rj en futuro meter cache
////        if (!this.chapterCache.isExpired() && this.chapterCache.isCached(userId)) {
////            userDataStore = DiskChapterDataStore(this.chapterCache)
////        } else {
//            chapterDataStore = createCloudDataStore()
////        }
//
//        return chapterDataStore
//    }

    /**
     * Create [ChapterDataStore] to retrieve data from the Cloud.
     */
    fun createCloudDataStore(): ChapterDataStore {
        val chapterEntityJsonMapper = ChapterEntityJsonMapper()
        val restApi = RestApiImpl(this.context, chapterEntityJsonMapper)

        return CloudChapterDataStore(restApi/*, this.chapterCache*/)
    }
}