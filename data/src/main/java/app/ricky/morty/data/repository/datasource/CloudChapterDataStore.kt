package app.ricky.morty.data.repository.datasource

import app.ricky.morty.data.entity.response.chapter.ChapterResponseEntity
import app.ricky.morty.data.entity.response.character.CharacterListResponseEntity
import app.ricky.morty.data.net.RestApi
import io.reactivex.Observable

class CloudChapterDataStore constructor(private val restApi: RestApi/*, val userCache: ChapterCache*/): ChapterDataStore {

    override fun chapterEntityList(next: String): Observable<ChapterResponseEntity> {
        return this.restApi.chapterEntityList(next)
    }

    override fun characterEntityList(characterIds: String): Observable<CharacterListResponseEntity> {
        return this.restApi.characterEntityList(characterIds)
    }
}