package app.ricky.morty.data.entity.response.character

class CharacterListResponseEntity (val characterList: Collection<CharacterEntity>)