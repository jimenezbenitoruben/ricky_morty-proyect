package app.ricky.morty.data.net

import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL
import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit


class ApiConnection @Throws(MalformedURLException::class) constructor(urlString : String): Callable<String>{

    private var response: String? = null
    private val url: URL = URL(urlString)

    /**
     * Do a request to an api synchronously.
     * It should not be executed in the main thread of the application.
     *
     * @return A string response
     */
    fun requestSyncCall(): String? {
        connectToApi()
        return response
    }

    private fun connectToApi() {
        val okHttpClient = this.createClient()
        val request = Request.Builder()
                .url(this.url)
                .addHeader(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_JSON)
                .get()
                .build()

        try {
            this.response = okHttpClient.newCall(request).execute().body().string()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    private fun createClient(): OkHttpClient {
        val okHttpClient = OkHttpClient()
        okHttpClient.setReadTimeout(10000, TimeUnit.MILLISECONDS)
        okHttpClient.setConnectTimeout(15000, TimeUnit.MILLISECONDS)

        return okHttpClient
    }

    @Throws(Exception::class)
    override fun call(): String? {
        return requestSyncCall()
    }

    companion object {

        private const val CONTENT_TYPE_LABEL = "Content-Type"
        private const val CONTENT_TYPE_VALUE_JSON = "application/json; charset=utf-8"

        @Throws(MalformedURLException::class)
        fun createGET(url: String): ApiConnection {
            return ApiConnection(url)
        }
    }
}
