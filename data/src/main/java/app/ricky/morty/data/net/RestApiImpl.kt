package app.ricky.morty.data.net

import android.content.Context
import android.net.ConnectivityManager
import app.ricky.morty.data.entity.mapper.ChapterEntityJsonMapper
import app.ricky.morty.data.entity.response.chapter.ChapterResponseEntity
import app.ricky.morty.data.entity.response.character.CharacterListResponseEntity
import app.ricky.morty.data.exception.NetworkConnectionException
import io.reactivex.Observable
import java.net.MalformedURLException

class RestApiImpl(context: Context?, private var chapterEntityJsonMapper: ChapterEntityJsonMapper) : RestApi {

    var context: Context

    init {
        if (context == null) {
            throw IllegalArgumentException("The constructor parameters cannot be null!!!")
        }
        this.context = context.applicationContext
    }

    override fun chapterEntityList(next: String): Observable<ChapterResponseEntity> {
        return Observable.create<ChapterResponseEntity> { emitter ->
            if (isThereInternetConnection()) {
                try {
                    val responseChapterEntities = getChapterEntitiesFromApi(next)
                    if (responseChapterEntities != null) {
                        emitter.onNext(chapterEntityJsonMapper.transformChapterEntityCollection(
                                responseChapterEntities))
                        emitter.onComplete()
                    } else {
                        emitter.onError(NetworkConnectionException())
                    }
                } catch (e: Exception) {
                    emitter.onError(NetworkConnectionException(e.cause))
                }

            } else {
                emitter.onError(NetworkConnectionException())
            }
        }
    }

    override fun characterEntityList(characterIds: String): Observable<CharacterListResponseEntity> {
        return Observable.create<CharacterListResponseEntity> { emitter ->
            if (isThereInternetConnection()) {
                try {
                    val responseCharacterEntities = getCharacterListFromApi(characterIds)
                    if (responseCharacterEntities != null) {
                        emitter.onNext(chapterEntityJsonMapper.transformCharacterListResponseEntity(
                                responseCharacterEntities))
                        emitter.onComplete()
                    } else {
                        emitter.onError(NetworkConnectionException())
                    }
                } catch (e: Exception) {
                    emitter.onError(NetworkConnectionException(e.cause))
                }

            } else {
                emitter.onError(NetworkConnectionException())
            }
        }    }

    @Throws(MalformedURLException::class)
    private fun getChapterEntitiesFromApi(next: String): String? {
        return if(next.isEmpty()){
            ApiConnection.createGET(API_URL_GET_CHAPTER_LIST).requestSyncCall()
        }else{
            ApiConnection.createGET(next).requestSyncCall()
        }
    }

    @Throws(MalformedURLException::class)
    private fun getCharacterListFromApi(characterIds: String): String? {
        return ApiConnection.createGET(API_URL_GET_CHARACTER_LIST + characterIds).requestSyncCall()
    }

    private fun isThereInternetConnection(): Boolean {
        val isConnected: Boolean

        val connectivityManager = this.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting

        return isConnected
    }

    companion object {
        private const val API_BASE_URL = "https://rickandmortyapi.com/api/"

        /** Api url for getting all users  */
        const val API_URL_GET_CHAPTER_LIST = API_BASE_URL + "episode"

        /** Api url for getting all characters: Remember to concatenate all character ids*/
        const val API_URL_GET_CHARACTER_LIST = API_BASE_URL + "character/"
    }
}