package app.ricky.morty.data.entity.response.chapter

data class InfoEntity (val count: Long, val pages: Long, val next: String, val prev: String)