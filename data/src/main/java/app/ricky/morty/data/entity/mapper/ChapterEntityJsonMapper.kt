package app.ricky.morty.data.entity.mapper

import app.ricky.morty.data.entity.response.chapter.ChapterResponseEntity
import app.ricky.morty.data.entity.response.character.CharacterEntity
import app.ricky.morty.data.entity.response.character.CharacterListResponseEntity
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import javax.inject.Inject

class ChapterEntityJsonMapper @Inject constructor() {

    private val gson: Gson = Gson()

    /**
     * Transform from valid json string to [CharacterListResponseEntity].
     *
     * @param characterListJsonResponse A json representing a user profile.
     * @return [CharacterListResponseEntity].
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    @Throws(JsonSyntaxException::class)
    fun transformCharacterListResponseEntity(characterListJsonResponse: String): CharacterListResponseEntity {
        val collectionType = object : TypeToken<Collection<CharacterEntity>>() {}.type
        val enums = gson.fromJson(characterListJsonResponse, collectionType) as Collection<CharacterEntity>

        return CharacterListResponseEntity(enums)
    }

    /**
     * Transform from valid json string to [ChapterResponseEntity].
     *
     * @param chapterListJsonResponse A json representing a collection of chapters.
     * @return [ChapterResponseEntity].
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    @Throws(JsonSyntaxException::class)
    fun transformChapterEntityCollection(chapterListJsonResponse: String): ChapterResponseEntity? {
        val listOfChapterEntityType = object : TypeToken<ChapterResponseEntity>() {}.type

        return  this.gson.fromJson<ChapterResponseEntity>(chapterListJsonResponse, listOfChapterEntityType)
    }
}
