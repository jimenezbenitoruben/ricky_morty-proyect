package app.ricky.morty.data.repository

import app.ricky.morty.data.entity.mapper.ChapterEntityDataMapper
import app.ricky.morty.data.repository.datasource.ChapterDataStoreFactory
import app.ricky.morty.domain.`object`.chapter.ChapterResponse
import app.ricky.morty.domain.`object`.character.CharacterListResponse
import app.ricky.morty.domain.repository.ChapterRepository
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChapterDataRepository @Inject constructor(private val chapterDataStoreFactory: ChapterDataStoreFactory, private val chapterEntityDataMapper: ChapterEntityDataMapper) : ChapterRepository {

    override fun chapters(next: String): Observable<ChapterResponse> {
        //we always get all users from the cloud
        val chapterDataStore = this.chapterDataStoreFactory.createCloudDataStore()
        return chapterDataStore.chapterEntityList(next).map(this.chapterEntityDataMapper::transform)
    }

    override fun characters(charactersIds: String): Observable<CharacterListResponse> {
        //we always get all characters from the cloud
        val chapterDataStore = this.chapterDataStoreFactory.createCloudDataStore()
        return chapterDataStore.characterEntityList(charactersIds).map(this.chapterEntityDataMapper::transform)    }

}