package app.ricky.morty.data.entity.mapper

import app.ricky.morty.data.entity.response.chapter.ChapterEntity
import app.ricky.morty.data.entity.response.chapter.ChapterResponseEntity
import app.ricky.morty.data.entity.response.chapter.InfoEntity
import app.ricky.morty.data.entity.response.character.CharacterEntity
import app.ricky.morty.data.entity.response.character.CharacterListResponseEntity
import app.ricky.morty.domain.`object`.chapter.Chapter
import app.ricky.morty.domain.`object`.chapter.ChapterResponse
import app.ricky.morty.domain.`object`.chapter.Info
import app.ricky.morty.domain.`object`.character.Character
import app.ricky.morty.domain.`object`.character.CharacterListResponse
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChapterEntityDataMapper @Inject constructor() {

    /**
     * Transform a [ChapterEntity] into an [Chapter].
     *
     * @param chapterEntity Object to be transformed.
     * @return [Chapter] if valid [ChapterEntity] otherwise null.
     */
    private fun transform(chapterEntity: ChapterEntity?): Chapter? {
        var chapter: Chapter? = null
        if (chapterEntity != null) {
            chapter = Chapter(chapterEntity.id,
                    chapterEntity.name,
                    chapterEntity.airDate,
                    chapterEntity.episode,
                    chapterEntity.characters,
                    chapterEntity.url,
                    chapterEntity.created)
        }
        return chapter
    }

    /**
     * Transform a List of [ChapterEntity] into a Collection of [Chapter].
     *
     * @param chapterEntityCollection Object Collection to be transformed.
     * @return [Chapter] if valid [ChapterEntity] otherwise null.
     */
    private fun transform(chapterEntityCollection: Collection<ChapterEntity>): List<Chapter> {
        val chapterList = ArrayList<Chapter>()
        for (chapterEntity in chapterEntityCollection) {
            val chapter = transform(chapterEntity)
            if (chapter != null) {
                chapterList.add(chapter)
            }
        }
        return chapterList
    }

    private fun transform(infoEntity: InfoEntity?): Info? {
        var info: Info? = null

        if (infoEntity != null) {
            info = Info(infoEntity.count,
                    infoEntity.pages,
                    infoEntity.next,
                    infoEntity.prev)
        }
        return info
    }

    fun transform(chapterResponseEntity: ChapterResponseEntity): ChapterResponse {
        val chapterResponse: ChapterResponse?

         //Create a default one if not coming
         val info = transform(chapterResponseEntity.info) ?: Info(0,0,"","")
         chapterResponse = ChapterResponse(
                 info,
                 transform(chapterResponseEntity.results))

        return chapterResponse
    }

    private fun transform(characterEntity: CharacterEntity?): Character {
        if (characterEntity == null) {
            throw IllegalArgumentException("Cannot transform a null value")
        }

        return Character(characterEntity.id,
                characterEntity.name,
                characterEntity.status,
                characterEntity.species,
                characterEntity.type,
                characterEntity.gender,
                characterEntity.image,
                characterEntity.episode,
                characterEntity.url,
                characterEntity.created)
    }

    private fun transform(characterEntityList: Collection<CharacterEntity>?): Collection<Character>  {

        val characterList = ArrayList<Character>()

        if (characterEntityList != null && !characterEntityList.isEmpty()) {
            for (character in characterEntityList) {
                characterList.add(transform(character))
            }
        }

        return characterList
    }


    fun transform(characterListResponseEntity: CharacterListResponseEntity): CharacterListResponse {
        val characterListResponse: CharacterListResponse?

        characterListResponse = CharacterListResponse(
                transform(characterListResponseEntity.characterList))

        return characterListResponse
    }

}
