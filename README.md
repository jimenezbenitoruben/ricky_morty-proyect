Prueba hecha con el API publica [The Rick and Morty API](https://rickandmortyapi.com/) 

## Primera pantalla

La primera pantalla de la aplicación mostrará todos los episodios de Rick y Morty publicados hasta la fecha. 
Los episodios deberán estar agrupados por temporadas en diferentes secciones, conteniendo la siguiente información:

 - Información:
    - para cada temporada, un título identificativo (por ejemplo, “Temporada 1”, “Temporada 2”, etc.).
    - para cada episodio, el código y el título de dicho episodio.


## Segunda pantalla

Al pulsar en cualquiera de los episodios de la primera pantalla, se transitará a la segunda pantalla.
Esta contendrá, de nuevo, el código y el título del episodio, además de todos aquellos personajes que aparezcan en dicho episodio.

- Para cada personaje se mostrará la siguiente información:
	- imagen del personaje
	- nombre del personaje
	- especie a la que pertenece
	- status actual, es decir: vivo, muerto o desconocido

Además, en esta segunda pantalla se deberá ofrecer al usuario algún mecanismo de  filtrado de personajes de acuerdo a su status. 
Los valores para filtrar personajes serán: vivos, muertos, desconocido o todos, que será valor por defecto y mostrará todos los personajes del episodio, independientemente de su status.
