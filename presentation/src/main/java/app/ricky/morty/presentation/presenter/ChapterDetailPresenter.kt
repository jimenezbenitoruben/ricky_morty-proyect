package app.ricky.morty.presentation.presenter

import app.ricky.morty.domain.`object`.character.CharacterListResponse
import app.ricky.morty.domain.interactor.DefaultObserver
import app.ricky.morty.domain.interactor.GetCharacterList
import app.ricky.morty.presentation.internal.di.PerActivity
import app.ricky.morty.presentation.mapper.CharacterListModelDataMapper
import app.ricky.morty.presentation.model.chapter.ChapterModel
import app.ricky.morty.presentation.view.ChapterDetailView
import javax.inject.Inject

@PerActivity
class ChapterDetailPresenter @Inject constructor(private val getCharacterListUseCase: GetCharacterList, private val characterListModelDataMapper: CharacterListModelDataMapper): Presenter {
    var chapterDetailView: ChapterDetailView? = null

    override fun resume() {}

    override fun pause() {}

    override fun destroy() {
        this.getCharacterListUseCase.dispose()
        this.chapterDetailView = null
    }

    /**
     * Initializes the presenter by start retrieving the user list.
     */
    fun initialize(chapterModel: ChapterModel) {
        this.loadChapterDetail(chapterModel)
    }

    /**
     * Loads chapter detail
     */
    private fun loadChapterDetail(chapterModel: ChapterModel) {
        this.hideViewRetry()
        this.showViewLoading()
        this.setChapterCode(chapterModel.episode)
        this.setChapterTitle(chapterModel.name)
        this.getCharacterList(chapterModel.characters)
    }

    private fun showViewRetry() {
        this.chapterDetailView?.showRetry()
    }

    private fun hideViewRetry() {
        this.chapterDetailView?.hideRetry()
    }

    private fun showViewLoading() {
        this.chapterDetailView?.showLoading()
    }

    private fun hideViewLoading() {
        this.chapterDetailView?.hideLoading()
    }

    private fun showErrorMessage(/*errorBundle: ErrorBundle*/) {
//        val errorMessage = ErrorMessageFactory.create(this.viewListView.context(),
//                errorBundle.getException())
        this.chapterDetailView?.showError("errorMessage")
    }

    private fun setChapterCode(code: String) {
        this.chapterDetailView?.setChapterCode(code)

    }

    private fun setChapterTitle(name: String) {
        this.chapterDetailView?.setChapterTitle(name)
    }

    private fun getCharacterList(characterList: List<String>) {
        val characterIds = generateCharacterIds(characterList)

        this.getCharacterListUseCase.execute(CharacterListObserver(), GetCharacterList.Params.forCharacters(characterIds))
    }

    private fun generateCharacterIds(characterList: List<String>) : String{
        var characterIds = ""
        for(characterUrl in characterList){
            val characterId = characterUrl.substringAfter("character/")
            if(!characterIds.isEmpty()){
                characterIds += ","
            }
            characterIds += characterId
        }
        return characterIds
    }

    private inner class CharacterListObserver : DefaultObserver<CharacterListResponse>() {

        override fun onComplete() {
            this@ChapterDetailPresenter.hideViewLoading()
        }

        override fun onError(exception: Throwable) {
            this@ChapterDetailPresenter.hideViewLoading()
            this@ChapterDetailPresenter.showErrorMessage(/*DefaultErrorBundle(e as Exception)*/)
            this@ChapterDetailPresenter.showViewRetry()
        }

        override fun onNext(t: CharacterListResponse) {
            this@ChapterDetailPresenter.showChaptersCollectionInView(t)
        }
    }

    private fun showChaptersCollectionInView(characterListResponse: CharacterListResponse) {
        val characterListResponseModel = this.characterListModelDataMapper.transform(characterListResponse)

        this.chapterDetailView?.renderCharacterList(characterListResponseModel.characterList)
    }
}