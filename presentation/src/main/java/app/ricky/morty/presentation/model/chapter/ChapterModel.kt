package app.ricky.morty.presentation.model.chapter

import java.io.Serializable

data class ChapterModel(val id: Long,
                        val name: String,
                        val airDate: String,
                        val episode: String,
                        val characters: List<String>,
                        val url: String,
                        val created: String) : Serializable