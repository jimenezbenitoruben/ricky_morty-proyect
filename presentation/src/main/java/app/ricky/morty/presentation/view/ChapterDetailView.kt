package app.ricky.morty.presentation.view

import app.ricky.morty.presentation.model.character.CharacterModel

interface ChapterDetailView : LoadDataView {
    fun setChapterCode(code: String)
    fun setChapterTitle(title: String)

    fun renderCharacterList(characterList: Collection<CharacterModel>)
}