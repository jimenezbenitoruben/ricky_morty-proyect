package app.ricky.morty.presentation.internal.di.components

import android.content.Context
import app.ricky.morty.domain.executor.PostExecutionThread
import app.ricky.morty.domain.executor.ThreadExecutor
import app.ricky.morty.domain.repository.ChapterRepository
import app.ricky.morty.presentation.internal.di.modules.ApplicationModule
import app.ricky.morty.presentation.view.activitiy.BaseActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun inject(baseActivity: BaseActivity)

    //Exposed to sub-graphs.
    fun context(): Context
    fun threadExecutor(): ThreadExecutor
    fun postExecutionThread(): PostExecutionThread
    fun chapterRepository(): ChapterRepository

}