package app.ricky.morty.presentation.internal.di.modules

import android.app.Activity
import app.ricky.morty.presentation.internal.di.PerActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private var activity: Activity) {

    @Provides
    @PerActivity
    fun activity() : Activity = activity
}
