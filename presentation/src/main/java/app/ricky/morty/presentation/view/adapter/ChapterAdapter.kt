package app.ricky.morty.presentation.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import app.ricky.morty.presentation.R
import app.ricky.morty.presentation.model.chapter.ChapterModel
import kotlinx.android.synthetic.main.row_chapter.view.*
import kotlinx.android.synthetic.main.row_footer.view.*
import javax.inject.Inject

class ChapterAdapter @Inject constructor(val context: Context) : RecyclerView.Adapter<ChapterAdapter.ChapterViewHolder>() {

    private var itemsCollection: ArrayList<Any> = ArrayList()
    private var itemsTypes: ArrayList<Int> = ArrayList()

    private var layoutInflater = (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater)

    var onItemClickListener: OnItemClickListener? = null

    companion object {
        const val TYPE_SEASON = 0
        const val TYPE_CHAPTER = 1
        const val TYPE_FOOTER = 2
    }

    interface OnItemClickListener{
        fun onChapterClicked(chapterModel: ChapterModel?)
        fun onLoadMoreClicked(next: String)

    }

    override fun getItemCount(): Int {
        return this.itemsCollection.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChapterAdapter.ChapterViewHolder {
        return when(viewType){
            TYPE_SEASON -> {
                val view = this.layoutInflater.inflate(R.layout.row_season, parent, false)
                ChapterViewHolder(view, viewType)
            }
            TYPE_FOOTER -> {
                val view = this.layoutInflater.inflate(R.layout.row_footer, parent, false)
                ChapterViewHolder(view, viewType)
            } else -> {
                val view = this.layoutInflater.inflate(R.layout.row_chapter, parent, false)
                ChapterViewHolder(view, viewType)
            }
        }
    }

    override fun onBindViewHolder(holder: ChapterAdapter.ChapterViewHolder, position: Int) {
        val viewType = getItemViewType(position)

        when(viewType) {
            TYPE_SEASON -> {
                val season = this.itemsCollection[position] as String
                holder.textViewTitle?.text = season
            }
            TYPE_FOOTER -> {
                holder.buttonLoadMore?.setOnClickListener {
                    this@ChapterAdapter.onItemClickListener?.onLoadMoreClicked(this.itemsCollection[position] as String)
                }
            }
            else -> {
                val chapterModel = this.itemsCollection[position] as ChapterModel
                holder.textViewTitle?.text = chapterModel.name
                holder.textViewCode?.text = chapterModel.episode
                holder.itemView.setOnClickListener {
                    this@ChapterAdapter.onItemClickListener?.onChapterClicked(chapterModel)
                }
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return itemsTypes[position]
    }

    class ChapterViewHolder(itemView: View, type :Int) : RecyclerView.ViewHolder(itemView) {
        var textViewTitle: TextView? = null
        var textViewCode: TextView? = null
        var buttonLoadMore: Button? = null

        init {
            when (type) {
                TYPE_SEASON -> {
                    textViewTitle = itemView.title
                }
                TYPE_FOOTER -> {
                    buttonLoadMore = itemView.buttonLoadMore
                }
                else -> {
                    textViewTitle = itemView.title
                    textViewCode = itemView.code
                }
            }
        }
    }

    fun setChaptersMap(chapterModelMap: Map<String, ArrayList<ChapterModel>>) {
        this.validateChaptersCollection(chapterModelMap)
        for((key, list) in chapterModelMap){
            val season = context.getString(R.string.season) + " " + key.substring(1,3)
            if(!itemsCollection.contains(season)){
                this.itemsCollection.add(season)
                this.itemsTypes.add(TYPE_SEASON)
            }
            for(item in list){
                this.itemsCollection.add(item)
                this.itemsTypes.add(TYPE_CHAPTER)
            }
        }
        this.notifyDataSetChanged()
    }

    private fun validateChaptersCollection(chapterModelMap: Map<String, ArrayList<ChapterModel>>?) {
        if (chapterModelMap == null) {
            throw IllegalArgumentException("The map cannot be null")
        }
    }

    fun updateLoadMoreVisibility(next: String) {
        if(next.isEmpty()){
            removeFooter()
        }else{
            addFooter(next)
        }
    }

    private fun removeFooter() {
        if(itemsTypes.contains(TYPE_FOOTER)){
            val footerIndex = itemsTypes.indexOf(TYPE_FOOTER)
            itemsCollection.removeAt(footerIndex)
            itemsTypes.removeAt(footerIndex)
        }
    }

    private fun addFooter(next: String) {
        itemsCollection.add(next)
        itemsTypes.add(TYPE_FOOTER)
    }
}