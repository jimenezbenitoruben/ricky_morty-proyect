package app.ricky.morty.presentation.model.chapter


data class ChapterResponseModel(val info: InfoModel, val results: Map<String, ArrayList<ChapterModel>>)