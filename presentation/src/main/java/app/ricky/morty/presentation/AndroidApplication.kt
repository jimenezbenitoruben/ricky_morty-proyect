package app.ricky.morty.presentation

import android.app.Application
import app.ricky.morty.presentation.internal.di.components.ApplicationComponent
import app.ricky.morty.presentation.internal.di.components.DaggerApplicationComponent
import app.ricky.morty.presentation.internal.di.modules.ApplicationModule

class AndroidApplication : Application() {
    lateinit var applicationComponent : ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        initializeInjector()
    }

    private fun initializeInjector() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()

    }

}