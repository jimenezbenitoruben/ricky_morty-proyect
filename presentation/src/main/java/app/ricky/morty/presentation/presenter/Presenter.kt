package app.ricky.morty.presentation.presenter

interface Presenter {
    fun resume()
    fun pause()
    fun destroy()
}