package app.ricky.morty.presentation.view.activitiy

import android.os.Bundle
import app.ricky.morty.presentation.R
import app.ricky.morty.presentation.internal.di.HasComponent
import app.ricky.morty.presentation.internal.di.components.ActivityComponent
import app.ricky.morty.presentation.internal.di.components.DaggerActivityComponent
import app.ricky.morty.presentation.model.chapter.ChapterModel
import app.ricky.morty.presentation.view.fragment.ChapterListFragment

class ChapterListActivity : BaseActivity(), HasComponent, ChapterListFragment.ChapterListListener {

    private lateinit var activityComponent : ActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        if(savedInstanceState == null){
            super.onCreate(savedInstanceState)
            initializeInjector()
        }else{
            initializeInjector()
            super.onCreate(savedInstanceState)
        }
        setContentView(R.layout.activity_chapter_list)

        addFragment(R.id.fragmentContainer, ChapterListFragment())
    }

    private fun initializeInjector() {
        activityComponent = DaggerActivityComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build()
    }

    override fun getComponent(): ActivityComponent {
        return activityComponent
    }

    override fun onChapterClicked(chapterModel: ChapterModel) {
        this.navigator.navigateToChapterDetails(this, chapterModel)
    }
}
