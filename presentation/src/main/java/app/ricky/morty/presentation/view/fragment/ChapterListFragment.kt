package app.ricky.morty.presentation.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import app.ricky.morty.presentation.R
import app.ricky.morty.presentation.model.chapter.ChapterModel
import app.ricky.morty.presentation.presenter.ChapterListPresenter
import app.ricky.morty.presentation.view.ChapterListView
import app.ricky.morty.presentation.view.adapter.ChapterAdapter
import kotlinx.android.synthetic.main.fragment_chapter_list.*
import kotlinx.android.synthetic.main.fragment_chapter_list.view.*
import javax.inject.Inject

class ChapterListFragment : BaseFragment(), ChapterListView{

    /**
     * Interface for listening chapter list events.
     */
    interface ChapterListListener {
        fun onChapterClicked(chapterModel: ChapterModel)
    }

    @Inject
    lateinit var chapterListPresenter: ChapterListPresenter

    @Inject
    lateinit var chapterAdapter: ChapterAdapter

    private var chapterListListener: ChapterListListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (activity is ChapterListListener) {
            this.chapterListListener = activity as ChapterListListener
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivityComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragmentView = inflater.inflate(R.layout.fragment_chapter_list, container, false)
        setupRecyclerView(fragmentView)
        return fragmentView    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.chapterListPresenter.chapterListView = this
        if (savedInstanceState == null) {
            this.loadUserList()
        }    }

    override fun onResume() {
        super.onResume()
        this.chapterListPresenter.resume()
    }

    override fun onPause() {
        super.onPause()
        this.chapterListPresenter.pause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        recyclerView.adapter = null
    }

    override fun onDestroy() {
        super.onDestroy()
        this.chapterListPresenter.destroy()
    }

    override fun onDetach() {
        super.onDetach()
        this.chapterListListener = null
    }

    override fun renderChapterMap(chapterModelMap: Map<String, ArrayList<ChapterModel>>) {
        this.chapterAdapter.setChaptersMap(chapterModelMap)
    }

    override fun viewChapter(chapterModel: ChapterModel) {
        if (this.chapterListListener != null) {
            this.chapterListListener!!.onChapterClicked(chapterModel)
        }
    }

    override fun updateLoadMoreVisibility(next: String) {
        this.chapterAdapter.updateLoadMoreVisibility(next)
    }

    override fun showLoading() {
        Toast.makeText(activity, "show loading", Toast.LENGTH_SHORT).show()
    }

    override fun hideLoading() {
        Toast.makeText(activity, "hide loading", Toast.LENGTH_SHORT).show()
    }

    override fun showRetry() {
        //TODO:Rj
    }

    override fun hideRetry() {
        //TODO:Rj
    }

    override fun showError(message: String) =
            Toast.makeText(activity, "error message + $message", Toast.LENGTH_SHORT).show()

    override fun context(): Context = activity!!.applicationContext


    /**
     * Loads all users.
     */
    private fun loadUserList() {
        this.chapterListPresenter.initialize()
    }

    private fun setupRecyclerView(fragmentView: View) {
        this.chapterAdapter.onItemClickListener = onItemClickListener
        fragmentView.recyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL ,false)
        fragmentView.recyclerView.adapter = chapterAdapter
    }


    private val onItemClickListener = object : ChapterAdapter.OnItemClickListener {
        override fun onLoadMoreClicked(next: String) {
            this@ChapterListFragment.chapterListPresenter.loadMoreChapterList(next)
        }

        override fun onChapterClicked(chapterModel: ChapterModel?) {
            if (chapterModel != null) {
                this@ChapterListFragment.chapterListPresenter.onChapterClicked(chapterModel)
//                this@ChapterListFragment.userListPresenter.onUserClicked(chapterModel)
            }
        }
    }
}