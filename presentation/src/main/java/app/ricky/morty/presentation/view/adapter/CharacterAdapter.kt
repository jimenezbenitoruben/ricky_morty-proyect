package app.ricky.morty.presentation.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.ricky.morty.presentation.R
import app.ricky.morty.presentation.model.character.CharacterModel
import app.ricky.morty.presentation.utils.images.ImageLoaderImpl
import kotlinx.android.synthetic.main.row_character.view.*
import javax.inject.Inject

class CharacterAdapter @Inject constructor(val context: Context) : RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    @Inject
    lateinit var mImageLoader : ImageLoaderImpl

    private var characterCollection: ArrayList<CharacterModel> = ArrayList()

    private var layoutInflater: LayoutInflater = (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater)

    override fun getItemCount(): Int {
        return this.characterCollection.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterAdapter.CharacterViewHolder {
        val view = this.layoutInflater.inflate(R.layout.row_character, parent, false)
        return CharacterViewHolder(view)
    }

    override fun onBindViewHolder(holder: CharacterAdapter.CharacterViewHolder, position: Int) {
        val character = this.characterCollection[position]
        if(holder.ivCharacter != null){
            mImageLoader.load(holder.ivCharacter!!, character.image, true)
        }
        holder.tvName?.text = character.name
        holder.tvStatus?.text = character.status
        holder.tvSpecies?.text = character.species
    }

    class CharacterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ivCharacter: ImageView? = null
        var tvName: TextView? = null
        var tvStatus: TextView? = null
        var tvSpecies: TextView? = null

        init {
            ivCharacter = itemView.ivCharacter
            tvName = itemView.tvName
            tvStatus = itemView.tvStatus
            tvSpecies = itemView.tvSpecies
        }
    }

    fun setCharacterList(characterList: Collection<CharacterModel>) {
        this.characterCollection = characterList as ArrayList<CharacterModel>
        this.notifyDataSetChanged()
    }

}