package app.ricky.morty.presentation.presenter

import app.ricky.morty.domain.`object`.chapter.ChapterResponse
import app.ricky.morty.domain.interactor.DefaultObserver
import app.ricky.morty.domain.interactor.GetChapterList
import app.ricky.morty.presentation.internal.di.PerActivity
import app.ricky.morty.presentation.mapper.ChapterModelDataMapper
import app.ricky.morty.presentation.model.chapter.ChapterModel
import app.ricky.morty.presentation.view.ChapterListView
import javax.inject.Inject

@PerActivity
class ChapterListPresenter @Inject constructor(private val getChapterListUseCase: GetChapterList, private val chapterModelDataMapper: ChapterModelDataMapper): Presenter {
    var chapterListView : ChapterListView? = null

    override fun resume() {}

    override fun pause() {}

    override fun destroy() {
        this.getChapterListUseCase.dispose()
        this.chapterListView = null
    }

    /**
     * Initializes the presenter by start retrieving the user list.
     */
    fun initialize() {
        this.loadChapterList()
    }

    /**
     * Loads all users.
     */
    private fun loadChapterList() {
        this.hideViewRetry()
        this.showViewLoading()
        this.getChapterList()
    }

    /**
     * Loads more users.
     */
    fun loadMoreChapterList(next: String) {
        this.showViewLoading()
        this.getChapterList(next)
    }

    private fun showViewRetry() {
        this.chapterListView?.showRetry()
    }

    private fun hideViewRetry() {
        this.chapterListView?.hideRetry()
    }

    private fun showViewLoading() {
        this.chapterListView?.showLoading()
    }

    private fun hideViewLoading() {
        this.chapterListView?.hideLoading()
    }

    private fun showErrorMessage(/*errorBundle: ErrorBundle*/) {
//        val errorMessage = ErrorMessageFactory.create(this.viewListView.context(),
//                errorBundle.getException())
        this.chapterListView?.showError("errorMessage")
    }

    private fun getChapterList(next: String = "") {
        this.getChapterListUseCase.execute(ChapterListObserver(), GetChapterList.Params.forNext(next))
    }

    private inner class ChapterListObserver : DefaultObserver<ChapterResponse>() {

        override fun onComplete() {
            this@ChapterListPresenter.hideViewLoading()
        }

        override fun onError(exception: Throwable) {
            this@ChapterListPresenter.hideViewLoading()
            this@ChapterListPresenter.showErrorMessage(/*DefaultErrorBundle(e as Exception)*/)
            this@ChapterListPresenter.showViewRetry()
        }

        override fun onNext(t: ChapterResponse) {
            this@ChapterListPresenter.showChaptersCollectionInView(t)
        }
    }

    private fun showChaptersCollectionInView(chapterResponse: ChapterResponse) {
        val chapterModelResponse = this.chapterModelDataMapper.transform(chapterResponse)

        this.chapterListView?.renderChapterMap(chapterModelResponse.results)
        this.chapterListView?.updateLoadMoreVisibility(chapterModelResponse.info.next)
    }

    fun onChapterClicked(chapterModel: ChapterModel) {
        this.chapterListView?.viewChapter(chapterModel)
    }

}