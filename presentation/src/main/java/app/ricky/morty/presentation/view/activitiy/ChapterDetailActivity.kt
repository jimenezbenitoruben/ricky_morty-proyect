package app.ricky.morty.presentation.view.activitiy

import android.content.Context
import android.content.Intent
import android.os.Bundle
import app.ricky.morty.presentation.R
import app.ricky.morty.presentation.internal.di.HasComponent
import app.ricky.morty.presentation.internal.di.components.ActivityComponent
import app.ricky.morty.presentation.internal.di.components.DaggerActivityComponent
import app.ricky.morty.presentation.model.chapter.ChapterModel
import app.ricky.morty.presentation.view.fragment.ChapterDetailFragment

class ChapterDetailActivity : BaseActivity(), HasComponent {

    companion object {
        const val INTENT_EXTRA_PARAM_CHAPTER_MODEL = "org.android10.INTENT_EXTRA_PARAM_CHAPTER_MODEL"

        fun getCallingIntent(context: Context, chapterModel: ChapterModel): Intent {
            val callingIntent = Intent(context, ChapterDetailActivity::class.java)
            callingIntent.putExtra(INTENT_EXTRA_PARAM_CHAPTER_MODEL, chapterModel)
            return callingIntent
        }
    }

    private lateinit var chapterModel: ChapterModel
    private lateinit var activityComponent : ActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chapter_detail)

        this.initializeActivity(savedInstanceState)
        initializeInjector()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putSerializable(INTENT_EXTRA_PARAM_CHAPTER_MODEL, this.chapterModel)
        super.onSaveInstanceState(outState)
    }

    /**
     * Initializes this activity.
     */
    private fun initializeActivity(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            this.chapterModel = intent.getSerializableExtra(INTENT_EXTRA_PARAM_CHAPTER_MODEL) as ChapterModel
            addFragment(R.id.fragmentContainer, ChapterDetailFragment.forChapter(chapterModel))
        } else {
            this.chapterModel = savedInstanceState.getSerializable(INTENT_EXTRA_PARAM_CHAPTER_MODEL) as ChapterModel
        }
    }

    private fun initializeInjector() {
        activityComponent = DaggerActivityComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build()
    }

    override fun getComponent(): ActivityComponent {
        return activityComponent
    }
}
