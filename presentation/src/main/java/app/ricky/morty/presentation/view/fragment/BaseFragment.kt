package app.ricky.morty.presentation.view.fragment

import android.support.v4.app.Fragment
import app.ricky.morty.presentation.internal.di.HasComponent
import app.ricky.morty.presentation.internal.di.components.ActivityComponent

abstract class BaseFragment : Fragment() {

    protected fun getActivityComponent() : ActivityComponent{
        return (activity as HasComponent).getComponent()
    }
}