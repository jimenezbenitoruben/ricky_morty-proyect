package app.ricky.morty.presentation.utils.images

import android.widget.ImageView
import java.io.File

interface ImageLoader {

    fun load(i: ImageView, res: Int, effect: Boolean)
    fun load(i: ImageView, file: File, effect: Boolean)
    fun load(i: ImageView, url: String, effect: Boolean)
}