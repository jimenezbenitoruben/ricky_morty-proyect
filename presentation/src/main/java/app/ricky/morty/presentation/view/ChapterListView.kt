package app.ricky.morty.presentation.view

import app.ricky.morty.presentation.model.chapter.ChapterModel

interface ChapterListView : LoadDataView {
    fun renderChapterMap(chapterModelMap: Map<String, ArrayList<ChapterModel>>)

    fun viewChapter(chapterModel: ChapterModel)
    fun updateLoadMoreVisibility(next: String)
}