package app.ricky.morty.presentation.model.chapter

data class InfoModel(val count: Long, val pages: Long, val next: String, val prev: String)