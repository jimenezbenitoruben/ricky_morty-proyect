package app.ricky.morty.presentation.mapper

import app.ricky.morty.domain.`object`.chapter.Chapter
import app.ricky.morty.domain.`object`.chapter.ChapterResponse
import app.ricky.morty.domain.`object`.chapter.Info
import app.ricky.morty.presentation.internal.di.PerActivity
import app.ricky.morty.presentation.model.chapter.ChapterModel
import app.ricky.morty.presentation.model.chapter.ChapterResponseModel
import app.ricky.morty.presentation.model.chapter.InfoModel
import java.util.*
import javax.inject.Inject

@PerActivity
class ChapterModelDataMapper @Inject constructor() {
    /**
     * Transform a [Chapter] into an [ChapterModel].
     *
     * @param chapter Object to be transformed.
     * @return [ChapterModel].
     */
    private fun transform(chapter: Chapter?): ChapterModel {
        if (chapter == null) {
            throw IllegalArgumentException("Cannot transform a null value")
        }

        return ChapterModel(chapter.id,
                chapter.name,
                chapter.airDate,
                chapter.episode,
                chapter.characters,
                chapter.url,
                chapter.created)
    }

    /**
     * Transform a Collection of [Chapter] into a Collection of [ChapterModel].
     *
     * @param chaptersCollection Objects to be transformed.
     * @return List of [ChapterModel].
     */
    private fun transform(chaptersCollection: Collection<Chapter>?): Map<String, ArrayList<ChapterModel>> {
        val chapterModelsMap = HashMap<String, ArrayList<ChapterModel>>()
        if (chaptersCollection != null && !chaptersCollection.isEmpty()) {
            for (chapter in chaptersCollection) {
                val season : String = chapter.episode.substring(0, 3)
                val seasonList = chapterModelsMap[season]
                if(seasonList == null){
                    chapterModelsMap[season] = arrayListOf(transform(chapter))
                }else{
                    seasonList.add(transform(chapter))
                }
            }
        }
        return chapterModelsMap.toSortedMap()
    }

    private fun transform(info: Info?): InfoModel? {
        var infoModel: InfoModel? = null

        if (info != null) {
            infoModel = InfoModel(info.count,
                    info.pages,
                    info.next,
                    info.prev)
        }
        return infoModel
    }

    fun transform(chapterResponse: ChapterResponse): ChapterResponseModel {
        val chapterResponseModel: ChapterResponseModel?

        //Create a default one if not coming
        val infoModel = transform(chapterResponse.info) ?: InfoModel(0,0,"","")
        chapterResponseModel = ChapterResponseModel(
                infoModel,
                transform(chapterResponse.results))

        return chapterResponseModel
    }
}
