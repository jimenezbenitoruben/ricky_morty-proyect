package app.ricky.morty.presentation.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import app.ricky.morty.presentation.R
import app.ricky.morty.presentation.model.chapter.ChapterModel
import app.ricky.morty.presentation.model.character.CharacterModel
import app.ricky.morty.presentation.presenter.ChapterDetailPresenter
import app.ricky.morty.presentation.view.ChapterDetailView
import app.ricky.morty.presentation.view.adapter.CharacterAdapter
import dagger.internal.Preconditions
import kotlinx.android.synthetic.main.fragment_chapter_detail.*
import kotlinx.android.synthetic.main.fragment_chapter_detail.view.*
import javax.inject.Inject

class ChapterDetailFragment: BaseFragment(), ChapterDetailView, AdapterView.OnItemSelectedListener {

    @Inject
    lateinit var chapterDetailPresenter: ChapterDetailPresenter

    @Inject
    lateinit var characterAdapter: CharacterAdapter

    private var originalCharacterList: Collection<CharacterModel>? = null

    companion object {
        const val PARAM_CHAPTER_MODEL = "param_chapter_model"

        const val ITEMS_SPINNER_ALL = "all"
        val ITEMS_SPINNER = arrayOf(ITEMS_SPINNER_ALL, "Alive", "Dead", "unknown")

        fun forChapter(chapterModel: ChapterModel): ChapterDetailFragment {
            val chapterDetailFragment = ChapterDetailFragment()
            val arguments = Bundle()
            arguments.putSerializable(PARAM_CHAPTER_MODEL, chapterModel)
            chapterDetailFragment.arguments = arguments
            return chapterDetailFragment
        }
    }

    init {
        retainInstance = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivityComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragmentView = inflater.inflate(R.layout.fragment_chapter_detail, container, false)
        setupRecyclerView(fragmentView)
        setupSpinner(fragmentView)
        return fragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.chapterDetailPresenter.chapterDetailView = this
        if (savedInstanceState == null) {
            this.loadChapterDetail()
        }    }

    override fun onResume() {
        super.onResume()
        this.chapterDetailPresenter.resume()
    }

    override fun onPause() {
        super.onPause()
        this.chapterDetailPresenter.pause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        recyclerView.adapter = null
    }

    override fun onDestroy() {
        super.onDestroy()
        this.chapterDetailPresenter.destroy()
    }

    override fun setChapterCode(code: String) {
        tvCode.text = code
    }

    override fun setChapterTitle(title: String) {
        tvTitle.text = title
    }

    override fun renderCharacterList(characterList: Collection<CharacterModel>) {
        originalCharacterList = characterList
        this.characterAdapter.setCharacterList(characterList)
    }

    override fun showLoading() {
        Toast.makeText(activity, "show loading", Toast.LENGTH_SHORT).show()
    }

    override fun hideLoading() {
        Toast.makeText(activity, "hide loading", Toast.LENGTH_SHORT).show()
    }

    override fun showRetry() {
        //TODO:Rj
    }

    override fun hideRetry() {
        //TODO:Rj
    }

    override fun showError(message: String) =
            Toast.makeText(activity, "error message + $message", Toast.LENGTH_SHORT).show()

    override fun context(): Context = activity!!.applicationContext


    /**
     * Loads chapter detail.
     */
    private fun loadChapterDetail() {
        this.chapterDetailPresenter.initialize(currentChapter())
    }

    /**
     * Get current user id from fragments arguments.
     */
    private fun currentChapter(): ChapterModel {
        val arguments = arguments
        Preconditions.checkNotNull(arguments, "Fragment arguments cannot be null")
        return arguments?.getSerializable(PARAM_CHAPTER_MODEL) as ChapterModel
    }

    private fun setupRecyclerView(fragmentView: View) {
        fragmentView.recyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL ,false)
        fragmentView.recyclerView.adapter = characterAdapter
    }

    private fun setupSpinner(fragmentView: View) {
        fragmentView.spinner.onItemSelectedListener = this
        fragmentView.spinner.adapter = ArrayAdapter(context(), R.layout.item_spinner, ITEMS_SPINNER)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if(originalCharacterList == null){
            return
        }
        ITEMS_SPINNER[position].let { status ->
            if(status == ITEMS_SPINNER_ALL){
                characterAdapter.setCharacterList(originalCharacterList!!)
            }else{
                characterAdapter.setCharacterList(originalCharacterList!!.filter { it.status == status })
            }
        }
    }
}