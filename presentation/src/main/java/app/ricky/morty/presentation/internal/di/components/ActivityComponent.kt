package app.ricky.morty.presentation.internal.di.components

import app.ricky.morty.presentation.internal.di.PerActivity
import app.ricky.morty.presentation.internal.di.modules.ActivityModule
import app.ricky.morty.presentation.view.fragment.ChapterDetailFragment
import app.ricky.morty.presentation.view.fragment.ChapterListFragment
import dagger.Component

@PerActivity
@Component(dependencies = [ApplicationComponent::class], modules = [ActivityModule::class])
interface ActivityComponent{
    fun inject(chapterListFragment: ChapterListFragment)
    fun inject(chapterDetailFragment: ChapterDetailFragment)
}