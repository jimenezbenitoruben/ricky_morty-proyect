package app.ricky.morty.presentation.navigator

import android.content.Context
import app.ricky.morty.presentation.model.chapter.ChapterModel
import app.ricky.morty.presentation.view.activitiy.ChapterDetailActivity
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Class used to navigate through the application.
 */
@Singleton
class Navigator @Inject constructor()//empty
{

    /**
     * Goes to the chapter details screen.
     *
     * @param context A Context needed to open the destiny activity.
     */
    fun navigateToChapterDetails(context: Context?, chapterModel: ChapterModel) {
        if (context != null) {
            val intentToLaunch = ChapterDetailActivity.getCallingIntent(context, chapterModel)
            context.startActivity(intentToLaunch)
        }
    }
}
