package app.ricky.morty.presentation.internal.di

import app.ricky.morty.presentation.internal.di.components.ActivityComponent

interface HasComponent{
    fun getComponent() : ActivityComponent
}