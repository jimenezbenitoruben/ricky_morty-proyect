package app.ricky.morty.presentation.utils.images

import android.widget.ImageView
import com.squareup.picasso.Picasso
import java.io.File
import javax.inject.Inject

class ImageLoaderImpl @Inject constructor() : ImageLoader{
    /**
     * Load image from resource
     */
    override fun load(i: ImageView, res: Int, effect: Boolean) {
        takeIf { effect }?.apply { Picasso.get().load(res).into(i) } ?: Picasso.get().load(res).noFade().into(i)
    }

    /**
     * Load image from file
     */
    override fun load(i: ImageView, file: File, effect: Boolean) {
        takeIf { effect }?.apply {  Picasso.get().load(file).into(i) } ?: Picasso.get().load(file).noFade().into(i)
    }

    /**
     * Load image from url (internet)
     */
    override fun load(i: ImageView, url: String, effect: Boolean) {
        takeIf { effect }?.apply { Picasso.get().load(url).into(i) } ?: Picasso.get().load(url).noFade().into(i)
    }
}