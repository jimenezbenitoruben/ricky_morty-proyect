package app.ricky.morty.presentation.model.character

data class CharacterListResponseModel(val characterList: Collection<CharacterModel>)