package app.ricky.morty.presentation.internal.di.modules

import android.content.Context
import app.ricky.morty.data.executor.JobExecutor
import app.ricky.morty.data.repository.ChapterDataRepository
import app.ricky.morty.domain.executor.PostExecutionThread
import app.ricky.morty.domain.executor.ThreadExecutor
import app.ricky.morty.domain.repository.ChapterRepository
import app.ricky.morty.presentation.AndroidApplication
import app.ricky.morty.presentation.UIThread
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(var application: AndroidApplication) {
    @Provides @Singleton fun provideContext() : Context = application

    @Provides @Singleton
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @Singleton
    internal fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread {
        return uiThread
    }
    @Provides @Singleton
    internal fun provideChapterRepository(chapterDataRepository: ChapterDataRepository): ChapterRepository {
        return chapterDataRepository
    }
}