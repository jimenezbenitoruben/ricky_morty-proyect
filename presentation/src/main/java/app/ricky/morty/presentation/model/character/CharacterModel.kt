package app.ricky.morty.presentation.model.character

data class CharacterModel(val id: Long,
                          val name: String,
                          val status: String,
                          val species: String,
                          val image: String)