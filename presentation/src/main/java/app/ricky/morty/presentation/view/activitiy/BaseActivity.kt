package app.ricky.morty.presentation.view.activitiy

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import app.ricky.morty.presentation.AndroidApplication
import app.ricky.morty.presentation.internal.di.components.ApplicationComponent
import app.ricky.morty.presentation.internal.di.modules.ActivityModule
import app.ricky.morty.presentation.navigator.Navigator
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getApplicationComponent().inject(this)
    }

    protected fun addFragment(containerViewId: Int, fragment: Fragment){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(containerViewId, fragment)
        fragmentTransaction.commit()
    }

    protected fun getApplicationComponent() : ApplicationComponent{
        return (application as AndroidApplication).applicationComponent
    }

    protected fun getActivityModule() : ActivityModule = ActivityModule(this)
}