package app.ricky.morty.presentation.mapper

import app.ricky.morty.domain.`object`.chapter.Chapter
import app.ricky.morty.domain.`object`.character.Character
import app.ricky.morty.domain.`object`.character.CharacterListResponse
import app.ricky.morty.presentation.internal.di.PerActivity
import app.ricky.morty.presentation.model.chapter.ChapterModel
import app.ricky.morty.presentation.model.character.CharacterListResponseModel
import app.ricky.morty.presentation.model.character.CharacterModel
import java.util.*
import javax.inject.Inject

@PerActivity
class CharacterListModelDataMapper  @Inject constructor(){

    private fun transform(character: Character?): CharacterModel {
        if (character == null) {
            throw IllegalArgumentException("Cannot transform a null value")
        }

        return CharacterModel(character.id,
                character.name,
                character.status,
                character.species,
                character.image)
    }

    /**
     * Transform a Collection of [Chapter] into a Collection of [ChapterModel].
     *
     * @param characterList Objects to be transformed.
     * @return List of [ChapterModel].
     */
    private fun transform(characterList: Collection<Character>?): Collection<CharacterModel>  {

        val characterListModel = ArrayList<CharacterModel>()

        if (characterList != null && !characterList.isEmpty()) {
            for (character in characterList) {
                characterListModel.add(transform(character))
            }
        }

        return characterListModel
    }


    fun transform(characterListResponse: CharacterListResponse): CharacterListResponseModel {
        val characterListResponseModel: CharacterListResponseModel?

        characterListResponseModel = CharacterListResponseModel(
                transform(characterListResponse.characterList))

        return characterListResponseModel
    }
}