package app.ricky.morty.domain.`object`.character

class CharacterListResponse(val characterList: Collection<Character>)