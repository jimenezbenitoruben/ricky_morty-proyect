package app.ricky.morty.domain.`object`.chapter

data class ChapterResponse(val info: Info, val results: List<Chapter>)
