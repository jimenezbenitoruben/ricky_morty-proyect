package app.ricky.morty.domain.interactor

import app.ricky.morty.domain.`object`.character.CharacterListResponse
import app.ricky.morty.domain.executor.PostExecutionThread
import app.ricky.morty.domain.executor.ThreadExecutor
import app.ricky.morty.domain.repository.ChapterRepository
import com.fernandocejas.arrow.checks.Preconditions
import io.reactivex.Observable
import javax.inject.Inject

class GetCharacterList @Inject constructor(private val chapterRepository: ChapterRepository, threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : UseCase<CharacterListResponse, GetCharacterList.Params>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: Params?): Observable<CharacterListResponse> {
        Preconditions.checkNotNull<Params>(params)
        return this.chapterRepository.characters(params?.characterIds ?: "")
    }

    class Params private constructor(val characterIds: String) {
        companion object {

            fun forCharacters(characterIds: String): Params {
                return Params(characterIds)
            }
        }
    }

}