package app.ricky.morty.domain.`object`.chapter

data class Chapter(
        val id: Long,
        val name: String,
        val airDate: String,
        val episode: String,
        val characters: List<String>,
        val url: String,
        val created: String)