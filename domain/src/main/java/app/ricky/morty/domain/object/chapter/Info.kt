package app.ricky.morty.domain.`object`.chapter

data class Info(val count: Long, val pages: Long, val next: String, val prev: String)