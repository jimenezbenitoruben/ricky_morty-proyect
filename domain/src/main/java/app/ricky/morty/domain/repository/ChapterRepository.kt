package app.ricky.morty.domain.repository

import app.ricky.morty.domain.`object`.chapter.ChapterResponse
import app.ricky.morty.domain.`object`.character.CharacterListResponse
import io.reactivex.Observable

interface ChapterRepository {
    fun chapters(next: String): Observable<ChapterResponse>

    fun characters(charactersIds: String): Observable<CharacterListResponse>

}