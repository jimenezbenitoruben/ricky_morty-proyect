package app.ricky.morty.domain.interactor

import app.ricky.morty.domain.`object`.chapter.ChapterResponse
import app.ricky.morty.domain.executor.PostExecutionThread
import app.ricky.morty.domain.executor.ThreadExecutor
import app.ricky.morty.domain.repository.ChapterRepository
import com.fernandocejas.arrow.checks.Preconditions
import io.reactivex.Observable
import javax.inject.Inject

class GetChapterList @Inject constructor(private val chapterRepository: ChapterRepository, threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : UseCase<ChapterResponse, GetChapterList.Params>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: Params?): Observable<ChapterResponse> {
        Preconditions.checkNotNull<Params>(params)
        return this.chapterRepository.chapters(params?.next ?: "")
    }

    class Params private constructor(val next: String) {
        companion object {

            fun forNext(next: String): Params {
                return Params(next)
            }
        }
    }

}
